#!/bin/bash

set -e

DEVICE=$1
[ -z "${DEVICE}" ] && echo "Device argument is mandatory: $0 /dev/sdX" && exit 1

sgdisk -Z "${DEVICE}"
sgdisk -o "${DEVICE}"
sgdisk "${DEVICE}" -n 1::+512M -t 1:ef00
sgdisk "${DEVICE}" -n 2::+4G -t 2:8200
sgdisk "${DEVICE}" -n 3

mkfs.fat -F 32 "${DEVICE}"1
mkfs.ext4 "${DEVICE}"3
mkswap "${DEVICE}"2
swapon "${DEVICE}"2

ROOTFS="/mnt"
mount "${DEVICE}"3 ${ROOTFS}
mkdir ${ROOTFS}/boot
mount "${DEVICE}"1 ${ROOTFS}/boot

pacstrap ${ROOTFS} base linux linux-firmware dhcp

genfstab -U ${ROOTFS} >> ${ROOTFS}/etc/fstab

ln -sf ${ROOTFS}/usr/share/zoneinfo/Europe/Belgrade ${ROOTFS}/etc/localtime
echo "en_US.UTF-8 UTF-8" >> ${ROOTFS}/etc/locale.gen
echo "LANG=en_US.UTF-8" >> ${ROOTFS}/etc/locale.conf

echo "arch" > ${ROOTFS}/etc/hostname
cat << EOF > ${ROOTFS}/etc/hosts
127.0.0.1       localhost
::1             localhost
127.0.1.1       arch.localdomain        localhost
EOF
cat << EOF > ${ROOTFS}/etc/systemd/network/20-wired.network
[Match]
Name=ens33

[Network]
DHCP=yes
EOF

mkdir -p ${ROOTFS}/boot/loader/entries
cat << EOF > ${ROOTFS}/boot/loader/entries/arch.conf
title   Arch
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=/dev/sda3 rw
EOF
echo "default arch-*" >> ${ROOTFS}/boot/loader/loader.conf

cat << EOF | arch-chroot ${ROOTFS}
set -e

hwclock --systohc
locale-gen

pacman -S --noconfirm posix base-devel bash-completion alsa-utils jq tmux vim git openssh xorg xorg-xinit xterm ttf-dejavu gtkmm3 open-vm-tools xf86-input-vmmouse xf86-video-vmware mesa imwheel docker docker-compose code chromium

systemctl enable systemd-networkd systemd-resolved sshd vmtoolsd.service vmware-vmblock-fuse.service docker.service containerd.service

bootctl --path=/boot install

passwd -d root
EOF

umount -R ${ROOTFS}