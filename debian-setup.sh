#!/bin/bash

set -e

DEVICE=$1
[ -z "${DEVICE}" ] && echo "Device argument is mandatory: $0 /dev/sdX" && exit 1

apt update && apt install -y debootstrap arch-install-scripts

sgdisk -Z "${DEVICE}"
sgdisk -o "${DEVICE}"
sgdisk "${DEVICE}" -n 1::+512M -t 1:ef00
sgdisk "${DEVICE}" -n 2::+4G -t 2:8200
sgdisk "${DEVICE}" -n 3

mkfs.fat -F 32 "${DEVICE}"1
mkfs.ext4 "${DEVICE}"3
mkswap "${DEVICE}"2
swapon "${DEVICE}"2

ROOTFS="/mnt/debinst"
mkdir ${ROOTFS}
mount "${DEVICE}"3 ${ROOTFS}
mkdir -p ${ROOTFS}/boot/efi
mount "${DEVICE}"1 ${ROOTFS}/boot/efi

debootstrap --variant=minbase --include init,systemd,dialog,locales,dhcpcd5,nvi,curl,wget --arch amd64 stable ${ROOTFS} http://deb.debian.org/debian

genfstab -U ${ROOTFS} >> ${ROOTFS}/etc/fstab

ln -sf ${ROOTFS}/usr/share/zoneinfo/Europe/Belgrade ${ROOTFS}/etc/localtime
echo "en_US.UTF-8 UTF-8" >> ${ROOTFS}/etc/locale.gen
echo 'LANG="en_US.UTF-8"' > ${ROOTFS}/etc/default/locale

echo "debian" > ${ROOTFS}/etc/hostname
cat << EOF > ${ROOTFS}/etc/hosts
127.0.0.1       localhost           debian
::1             localhost
127.0.1.1       debian.localdomain  localhost
EOF

cat << EOF > ${ROOTFS}/etc/systemd/network/20-wired.network
[Match]
Name=ens33

[Network]
DHCP=yes
EOF

cat << EOF | arch-chroot ${ROOTFS}
  set -e

  locale-gen
  dpkg-reconfigure --frontend=noninteractive locales tzdata

  apt install -y linux-image-amd64 systemd-resolved systemd-stub

  systemctl enable systemd-networkd systemd-resolved

  passwd -d root
EOF

umount -R ${ROOTFS}