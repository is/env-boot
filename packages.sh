#!/bin/bash

set -e


apt update && apt install -y sudo bash-completion build-essential nvi curl wget xorg open-vm-tools open-vm-tools-desktop git i3-wm i3status suckless-tools

usermod -G sudo,audio ivan

mkdir -p /etc/systemd/system/getty@tty1.service.d
cat << EOF > /etc/systemd/system/getty@tty1.service.d/override.conf
[Service]
Type=simple
ExecStart=
ExecStart=-/sbin/agetty --autologin ivan --noclear %I 
EOF
