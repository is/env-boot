#!/bin/bash

set -e

sed -r -i 's/^deb(.*)$/deb\1 contrib/g' /etc/apt/sources.list
apt update && apt install -y sudo bash-completion build-essential

useradd -m -G sudo,audio ivan -s /bin/bash
passwd -d ivan

mkdir -p /etc/systemd/system/getty@tty1.service.d
cat << EOF > /etc/systemd/system/getty@tty1.service.d/override.conf
[Service]
Type=simple
ExecStart=
ExecStart=-/sbin/agetty --autologin ivan --noclear %I 
EOF

apt install -y xorg open-vm-tools open-vm-tools-desktop git i3-wm i3status suckless-tools tmux cmake